---
title: Vorschlag II -- Darstellung und Aggregation der Daten aus dem Forschungsprogramm Argo
author: Sebastian Schmid - s0543196
---

# Themenvorschlag

Seit dem Jahr 2000 besteht das Argo-Programm. Dieses hat sich zur Aufgabe gemacht
ein Beobachtungssystem für die Weltmeere zu schaffen. Derzeit sind durch dieses
Programm mehr als 3900 Treibbojen in den Meeren dieses Planeten installiert.
Diese messen in regelmäßigen Abständen Temperatur, Salzgehalt und Dichte des
Meerwassers.  Die Daten des Argo-Programms stehen in "Echtzeit" jedem zur Verfügung.
Zwar gibt es bereits unzählige wissenschaftliche Puplikationen, doch hat sich
bis jetzt noch niemand mit einer Anwendergerechten Darstellung der Daten
beschäftigt.

Diese Arbeit wird diese Lücke schließen. Im Rahmen dieser Arbeit wird ein
kartografischer Webdienst entstehen. Der Dienst wird erlauben den Verlauf der
 Daten der Bojen aus einem spezifischen Kartenausschnitt darzustellen. Außerdem
 wird die ungefähre Position der Messroboter auf einer Karte angezeigt.

Um den Dienst zu erbringen müssen die Daten automatisiert erfasst und in eine
Datenbank eingepflegt werden. Durch dieses Backend werden die Daten lokal und
abfrage-optimiert für den Webdienst gespeichert.
