import os

from ._float import Float


#: The generator at this point is essential.
#:  At this point, the data is only a abstract model.
#:  So the calculations will not be executed before we need the data later
#    @DOC
#
# TODO: Think about: If it is a good idea the interface to the datasets is using an OOP-Pattern?
#   This produces massive overhead for something could be written in one line:
def data_sets_generator(data_dir):
    return (Float(os.path.join(data_dir, profile_directory)) for profile_directory in os.listdir(data_dir))


class ExtractorFactory:
    def __init__(self, data_directory):
        """
        Factory for Argo Float data-extractors.
        
        :param data_directory: PATH to the Argo_Data directory.
        """
        self.data_directory = data_directory

    def get_data_sets(self):
        """
        The whole dataset of the argo-floats.
        
        :return: Generator-object holding Argo-Floats
        """
        for profile_directory in os.listdir(self.data_directory):
            yield Float(os.path.join(self.data_directory, profile_directory))
