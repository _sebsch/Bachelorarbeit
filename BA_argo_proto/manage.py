from pprint import pprint

from flask_script import Manager, Server, Shell, prompt_bool

from data import db_writer
from webapp import app, db, gen_last_seen

assert db is not None

# https://flask-script.readthedocs.io/en/latest/

manager = Manager(app)


@manager.command
def rebuild_db():
    if prompt_bool("Are you sure you want to rebuild the database?"):
        db.drop_all()
        db.create_all()

        for argo_float in db_writer:
            argo_float.write_data()


@manager.command
def print_latest_locations():
    pprint(gen_last_seen())


manager.add_command("runserver", Server())
manager.add_command("shell", Shell())

if __name__ == "__main__":
    manager.run()
